import java.util.Stack;

public class Solution1 {
    public int evaluateExpression(String expression) {
        Stack<Integer> st = new Stack();
        int number = 0;
        char operator = '+';

        char ch[] = expression.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            if (Character.isDigit(ch[i]))
                number = number * 10 + ch[i] - '0';
            if (!Character.isDigit(ch[i]) && ch[i] != ' ' || i == ch.length - 1) {
                if (operator == '+')
                    st.push(number);
                else if (operator == '-')
                    st.push(-number);
                number = 0;
                operator = ch[i];

            }
        }

        int result = 0;
        while (!st.isEmpty()) {
            result += st.pop();
        }

        return result;
    }
}
