public class Solution3 {
    public ListNode reverseList(ListNode head) {
        ListNode reverse = null;
        ListNode current = head;

        while(current != null) {
            ListNode next = current.next;
            current.next = reverse;
            reverse = current;
            current = next;
        }
        return reverse;
    }

    public class ListNode {
        int value;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.value = val; }
        ListNode(int val, ListNode next) { this.value = val; this.next = next; }
    }
}
