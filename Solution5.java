import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution5 {
    public int lenOfLongSubArr(int[] array, int k) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(array);
        backtrack(list, new ArrayList<>(), array, 0);

        int max = 0;
        for (int i = 0; i < list.size(); i++) {
            List<Integer> listElement = list.get(i);
            if (sumOfListElements(listElement) == k) {
                max = Math.max(max, listElement.size());
            }
        }
        return max;
    }

    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int[] array, int start) {
        list.add(new ArrayList<>(tempList));
        for (int i = start; i < array.length; i++) {
            if (i > start && array[i] == array[i - 1])  continue;
            tempList.add(array[i]);
            backtrack(list, tempList, array, i + 1);
            tempList.remove(tempList.size() - 1);
        }
    }

    private int sumOfListElements(List<Integer> list) {
        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }
        return sum;
    }
}
