public class Solution6 {
    public boolean isValidSequence(int[] array, int[] sequence) {
        int arrayIndex = 0;
        int sequenceIndex = 0;
        while (sequenceIndex < sequence.length && arrayIndex < array.length) {
            if (sequence[sequenceIndex] == array[arrayIndex]) {
                sequenceIndex++;
            }
            arrayIndex++;
        }
        return sequenceIndex == sequence.length;
    }
}
