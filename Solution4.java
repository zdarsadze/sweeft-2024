import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Solution4 {
    public int[] findIntersection(int[] nums1, int[] nums2) {
        HashSet<Integer> nums1Set = new HashSet<>();
        HashSet<Integer> intersectionSet = new HashSet<>();
        for (int i = 0; i < nums1.length; i++) {
            nums1Set.add(nums1[i]);
        }
        for(int i = 0; i < nums2.length; i++) {
            if (nums1Set.contains(nums2[i])) {
                intersectionSet.add(nums2[i]);
            }
        }
        int intersection[] = new int[intersectionSet.size()];
        int j = 0;
        for (int n : intersectionSet) {
            intersection[j] = n;
            j++;
        }
        return intersection;
    }
}
