import java.util.List;

public class Solution2 {
    public int numberOfHappyStrings(List<String> strings) {
        int count = 0;
        for (String string : strings) {
            if (isHappyString(string)) {
                count++;
            }
        }
        return count;
    }

    private boolean isHappyString(String string) {
        for (int i = 0; i < string.length() - 1; i++) {
            if (string.charAt(i) == string.charAt(i + 1)) {
                return false;
            }
        }
        return true;
    }
}
